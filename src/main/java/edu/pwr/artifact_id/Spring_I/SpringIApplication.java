package edu.pwr.artifact_id.Spring_I;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringIApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringIApplication.class, args);
		System.out.println("Hello World!");
	}

}
